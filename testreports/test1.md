---
title: Report Ranger testing report
client: Ranging Automation
template: sample
author: Volkis
authoremail: info@volkis.com.au
authormobile: "Mobile Number"
vulndir: "vulnerabilities"
appendixdir: "appendices"
changes:
- [0.1, 2020-11-26, "Matthew Strahan", "Initial sample report"]
contributors:
- ["Matthew Strahan", "Volkis Managing Director", "mobile", "info@volkis.com.au"]
ra_methodology: true
fmtable:
- [ "Column 1", "Column 2", "Column 3" ]
- [ "First", "Second", "Third"]
- [ "1", "2", "3"]
- [ "Uno", "Dos", "Tres"]
fmtable_mix:
- [ "Column 1", "Column 2", "Column 3" ]
- [ "First", "Second", "Third"]
- Column 1: First
  Column 2: Second
  Column 3: Third
- "String"
dicttable:
- Column 1: First
  Column 2: Second
  Column 3: Third
- Column 1: 1
  Column 2: 2
  Column 3: 3
dicttable_uneven:
- Column 1: First
  Column 2: Second
  Column 3: Third
- Column 4: 1
  Column 1: 2
  Column 3: 3
dicttable_mix:
- Column 1: First
  Column 2: Second
  Column 3: Third
- [1,2,3]
- "String"
special: Parent special variable
---

{{ of.newsection() }}

# Overview

This is a testing report that tries to call things

{{ of.newpage() }}

## Default variables

List of default variables

- The highest risk is {{highestrisk}}
- Our company is {{company}} (defined in the template)
- The client is {{client}} (defined in report body)

## Including files

Include real file:

{{ include_file('includedfile.md') }}

Testing special variable outside of include file. Should show "Parent special variable":

* {{special}}

Include empty file:

{{ include_file('empty.md') }}

Include missing file:

{{ include_file('missing.md') }}


## Tables

Standard markdown table:

| Left Aligned | Centre Aligned | Right aligned |
| :----------- | :------------: | ------------: |
| First!       | Second!        | Third!        |

Formatted table:

{{ft()}}
| Left Aligned | Centre Aligned | Right aligned |
| :----------- | :------------: | ------------: |
| First!       | Second!        | Third!        |

Formatted table (no markdown afterwards):

{{ft()}}

Formatted table (incorrect markdown):

{{ft()}}
| Left Aligned | Centre Aligned | Right aligned |
| :------:----- | :------------: | :------------ |
| First!        Second!        | Third!        | s

Simple table:

{{ table(
[
  [ "Column 1", "Column 2" ],
  [ "First!", "Second!"]
]
) }}

Table with uneven rows:

{{ table(
[
  [ "Column 1", "Column 2", "Column 3" ],
  [ "First!", "Second!"],
  [ "1", "2", "3", "4" ]
]
) }}

Table from front matter:

{{ table(fmtable) }}

Table from front matter, all headings:

{{ table(fmtable, headings="top-left-right-bottom") }}

Table from front matter, rowspan and colspan:

{{ table(fmtable, headings="top-left-right-bottom", rowspan=[[],[1,2],[3]], colspan=[[2]]) }}

Table from front matter, overlapping rowspan and colspan:

{{ table(fmtable, headings="top-left-right-bottom", rowspan=[[],[2],[3]], colspan=[[2]]) }}

Table from front matter, empty colpicker and rowpicker:

{{ table(fmtable, headings="top-left-right-bottom", rowpicker=[], colpicker=[]) }}

Table from front matter, rowpicker colpicker repeat rows:

{{ table(fmtable, headings="top-left-right-bottom", rowpicker=[1,2,1,0], colpicker=[0,2,2]) }}

Table from front matter, rowpicker colpicker out of bound rows:

{{ table(fmtable, headings="top-left-right-bottom", rowpicker=[1,8,-1,0], colpicker=[0,-2,2]) }}

List table with dicts and strings as rows:

{{ table(fmtable_mix) }}

Dict table:

{{ table(dicttable) }}

Dict table with uneven assignment:

{{ table(dicttable_uneven) }}

Dict table with mix lists and strings:

{{ table(dicttable_mix) }}

## Charts

Sample chart:

{{of.fig("Sample chart", px.pie(values=[100,2000,550],names=['a','b','c']))}}

## Content Assistant

Counters:

- {{ counter() }}
- {{ counter() }}
- {{ counter() }}

Get counter should show the same value: {{ get_counter() }}

Named counter list:

- {{ counter("Mycounter") }}
- {{ counter("Mycounter") }}
- {{ counter("Mycounter") }}

List type counter list:

- {{ counter([1]) }}
- {{ counter([1]) }}
- {{ counter([1]) }}

Display CA table:

{{ display_table("mytable", header=["Col 1", "Col 2", "Col 3"], headings="top") }}

The actual rows are inputted below.

{{ table_row("mytable", ["First row!", "Let's do this!", "OK"]) }}
{{ table_row("mytable", ["Second row!", "Another!", "Yeah!"]) }}

Add table row to another table:

{{ table_row("secondtable", ["Second row!", "Another!", "Yeah!"]) }}

Bad table reference:

{{ display_table("missing", header=["Col 1", "Col 2", "Col 3"], headings="top") }}

