#!/usr/bin/env python3

import argparse
from vulningester import main

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Parses the output files of tools, matches them up to vulnerability write-up templates, puts the info from the tools in the front matter, and outputs them to a directory.")
    parser.add_argument('tooloutputs', metavar='file',
                        type=str, nargs='+', help='The output of a tool')
    parser.add_argument('-o', '--outputdir', type=str,
                        help='The vulnerabilities directory where the writeups will be placed.')
    parser.add_argument('-m', '--mapper', type=str,
                        default='', help='The mapper file')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='Turn on verbose mode.')
    parser.add_argument('-O', '--overwrite', action='store_true',
                        help='Overwrite vulnerabilities in output directory.')

    args = parser.parse_args()
    main(args)
