#!/usr/bin/env python3

import argparse
from report_ranger import config, main

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Report Manager collects together a bunch of markdown files to create a master markdown file that is suitable to use with Pandoc and Latex to build a PDF file.")
    parser.add_argument('-i', '--input', type=str, default=config.config['input_file'],
                        help='The main report file to process')
    parser.add_argument('-o', '--output', type=str, default=config.config['output_file'],
                        help='The file to output the final markdown to. If given a directory, the filename will be whatever is suggested by the report or template.')
    parser.add_argument('-f', '--format', type=str,
                        default='', help='The format to target (options are "latex", "pdf", "docx"). Defaults to whatever the extension tells us, otherwise {}.'.format(config.config['format']))
    parser.add_argument('-m', '--templatemapper', type=str, default='',
                        help='A template mapper file which holds a YAML mapping from a template name to a related file.')
    parser.add_argument('-t', '--template', type=str, default='',
                        help='The template file. Associated images should be in the same directory. Defaults to what is set in the report, otherwise: "{}"'.format(config.config['defaulttemplate']))
    parser.add_argument('-v', '--verbose', action='store_true', default=config.config['verbose'],
                        help=f'Turn on verbose mode. Default: {config.config["verbose"]}')

    args = parser.parse_args()
    main(args)