# Base image for poetry
FROM python:3.10-slim as python-base

ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1
ENV PIP_NO_CACHE_DIR=off
ENV PIP_DISABLE_PIP_VERSION_CHECK=on
ENV PIP_DEFAULT_TIMEOUT=100
ENV POETRY_HOME="/opt/poetry"
ENV POETRY_VIRTUALENVS_IN_PROJECT=true
ENV POETRY_NO_INTERACTION=1
ENV PYSETUP_PATH="/opt/pysetup"
ENV VENV_PATH="/opt/pysetup/.venv"

ENV PATH="$POETRY_HOME/bin:$VENV_PATH/bin:$PATH"

# Builder Image
FROM python-base as builder-base

RUN apt-get update && apt-get install --no-install-recommends -y curl build-essential

RUN curl -sSL https://install.python-poetry.org | python3 -

WORKDIR $PYSETUP_PATH
COPY poetry.lock pyproject.toml report_ranger ./

RUN poetry install --no-dev --no-root

FROM ubuntu:latest
WORKDIR /app

#Avoid getting prompted for tz during install
ENV TZ=Australia/Sydney
RUN ln -sf /usr/share/zoneinfo/$TZ /etc/localtime

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    texlive \
    texlive-latex-extra \
    texlive-fonts-extra \
    pandoc \
    lmodern \
    python3 \
    python3-pip \
    && rm -rf /var/lib/apt/lists/*

RUN mkdir /template
RUN mkdir /report

ENV POETRY_HOME="/opt/poetry"
ENV PYSETUP_PATH="/opt/pysetup"
ENV VENV_PATH="/opt/pysetup/.venv"
ENV PATH="$POETRY_HOME/bin:$VENV_PATH/bin:$PATH"

COPY --from=builder-base $PYSETUP_PATH $PYSETUP_PATH
COPY . .

# Fix a synlink issue
RUN ln -vfns /usr/bin/python3 /opt/pysetup/.venv/bin/python3

CMD ["python3", "reportranger.py"]
