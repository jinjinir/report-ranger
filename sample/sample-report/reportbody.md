---
title: Report Ranger sample report
client: Ranging Automation
template: sample
author: Volkis
authoremail: info@volkis.com.au
authormobile: "Mobile Number"
vulndir: "vulnerabilities"
appendixdir: "appendices"
changes:
- [0.1, 2020-11-26, "Matthew Strahan", "Initial sample report"]
contributors:
- ["Matthew Strahan", "Volkis Managing Director", "mobile", "info@volkis.com.au"]
ra_methodology: true
fmtable:
- [ "Column 1", "Column 2", "Column 3" ]
- [ "First", "Second", "Third"]
- [ "1", "2", "3"]
- [ "Uno", "Dos", "Tres"]
dicttable:
- Column 1: First
  Column 2: Second
  Column 3: Third
- Column 1: 1
  Column 2: 2
  Column 3: 3
import:
  vulnerabilities:
  - directory: vulnerabilities
    variable: vulnerabilities
  csv:
  - file: 'sampletable.csv'
    variable: csvtable
  xlsx:
  - file: 'sampletable.xlsx'
    variable: xlsxtable
    min_row: 0
    max_row: 3
    min_col: 0
    max_col: 2
---

{{ of.newsection() }}

# Overview

This is a sample report for Report Ranger that goes over some of the features of the system. It aims to show some of the features of Report Ranger as a guide.

Report Ranger reports are simple Markdown files. They will be pulled together by Report Ranger into a complete report, simplifying things like risk assessments, vulnerability lists, templated content, and automatically generated content. This report is best read alongside the source markdown files to get a good feel of how it all goes together.

Report Ranger was initially developed by Volkis for internal usage. It has since been open sourced and shared for outside use.

{{author}}

* Email: {{authoremail}}

{{ of.newpage() }}

# Tour of functionality

Outside of simply putting together vulnerability lists, Report Ranger has a whole bunch of functionality that you can take advantage of.

## Default variables

Report Ranger will automatically fill in some helpful variables for you:

- The highest risk is {{highestrisk}}
- Our company is {{company}} (defined in the template)
- The client is {{client}} (defined in report body)

The above list was generated with the following code:

{% raw %}
```
- The highest risk is {{highestrisk}}
- Our company is {{company}} (defined in the template)
- The client is {{client}} (defined in report body)
```
{% endraw %}

## Including files

You can include files with the following command:

{% raw %}
```
{{ include_file('includedfile.md') }}
```
{% endraw %}

{{ include_file('includedfile.md') }}

Let's try getting that variable again:

* {{special}}

Because special is set only in the included file, it can't be accessed here.

## Tables

Tables can be inputted using standard markdown tables. Here's a table:

| Column 1 | Column 2 |
| :------: | :------- |
| First!   | Second!  |

The code to make that table was:

```
| Column 1 | Column 2 |
| :------: | :------- |
| First!   | Second!  |
```

The problem with markdown, though, is that you can't make pretty tables. Headings have to be up top, you can't have different colours or formatting, no column specific alignment, or colspan or rowspan. You also can't set a table in the front matter as easily when you're putting directly in markdown.

There's two ways of fancy formatting for tables using Report Ranger. Firstly, you can use the `format_table()` or `ft()` function:

{{ft()}}
| Column 1 | Column 2 |
| :------: | :------- |
| First!   | Second!  |

{% raw %}
```
{{ft()}}
| Column 1 | Column 2 |
| :------: | :------- |
| First!   | Second!  |
```
{% endraw %}

You can also directly put in a table using the `table()` function. This is the same table:

{{ table(
[
  [ "Column 1", "Column 2" ],
  [ "First!", "Second!"]
]
) }}

{% raw %}
```
{{ table([
  [ "Column 1", "Column 2" ],
  [ "First!", "Second!"]
]) }}
```
{% endraw %}

With the `table()` function you can define the variable in the frontmatter:

{{ table(fmtable, headings='top', colalign=['c','l']) }}

With this table the variable is defined like this:

```
fmtable:
- [ "Column 1", "Column 2", "Column 3" ]
- [ "First", "Second", "Third"]
- [ "1", "2", "3"]
- [ "Uno", "Dos", "Tres"]
```

The table is then built using the `table()` function.

{% raw %}
```
{{ table(fmtable, headings='top', colalign=['c','l']) }}
```
{% endraw %}

A simpler way of defining lists in the frontmatter is often using dicts to create more human readable text. The table function can detect this and create a table from it:

{{ table(dicttable, headings='top') }}

With this table the variable is defined like this:

```
dicttable:
- Column 1: First
  Column 2: Second
  Column 3: Third
- Column 1: 1
  Column 2: 2
  Column 3: 3
```

The table is then built using the `table()` function.

{% raw %}
```
{{ table(dicttable, headings='top') }}
```
{% endraw %}

There's also the `rowpicker` and `colpicker`. These can be used to restrict what rows and columns are a part of the table:

{{ table(fmtable, headings='top', colalign=['c','l'], colpicker=[0,2], rowpicker=[1,2]) }}


{% raw %}
```
{{ table(fmtable, headings='top', colalign=['c','l'], colpicker=[0,2], rowpicker=[1,2]) }}
```
{% endraw %}

## Imports

Importing data from spreadsheets is a good way of putting in large tables without the fuss of having a huge variable in front matter.

You can have imports in two ways. Firstly importing using the "import" variable in the front matter:

{% raw %}
```
---
import:
  csv:
  - file: 'samplecsv.csv'
    variable: csvtable
---

{{ table(csvtable, headings='top') }}
```
{% endraw %}

{{ table(csvtable, headings='top') }}

In a similar way, XLSX files can also be imported:

{% raw %}
```
---
import:
  xlsx:
  - file: 'sampletable.xlsx'
    variable: xlsxtable
    min_row: 0
    max_row: 2
    min_col: 0
    max_col: 3
---

{{ table(xlsxtable, headings='top') }}
```
{% endraw %}

{{ table(xlsxtable, headings='top') }}

You can also use the `csv()` and `xlsx()` functions for importing directly into tables. The following is functionally the same as above.

{% raw %}
```
{{ table(csv('sampletable.csv'), headings='top') }}
```
{% endraw %}

{{ table(csv('sampletable.csv'), headings='top') }}

{% raw %}
```
{{ table(xlsx('sampletable.xlsx', min_row=0, max_row=2, min_col=0, max_col=3), headings='top') }}
```
{% endraw %}

{{ table(xlsx('sampletable.xlsx', min_row=0, max_row=2, min_col=0, max_col=3), headings='top') }}



## Charts

Charts can be dynamically generated. We use Plotly Express for generating charts. For instance, the next chart is generated with the following command:

{% raw %}
```
{{of.fig("Sample chart", px.pie(values=[100,2000,550],names=['a','b','c']))}}
```
{% endraw %}

{{of.fig("Sample chart", px.pie(values=[100,2000,550],names=['a','b','c']))}}

You can learn more at the [Plotly web site](https://plotly.com/python/plotly-express/).

## Content Assistant

Content Assistant allows you to have some state that is valid for the entire document. This currently can be used for counters and tables.

Calling `ca.counter()` will give you a document level counter. You can have as many counters as you want, for example `ca.counter('recommendations')` will give you a different counter to `ca.counter('parrots')`. This is a simple example of a counter at work:

- {{ ca.counter() }}
- {{ ca.counter() }}
- {{ ca.counter() }}

That was just:

{% raw %}
```
- {{ ca.counter() }}
- {{ ca.counter() }}
- {{ ca.counter() }}
```
{% endraw %}

You can dynamically generate a table throughout the document. This is useful for summary tables that are at the top of the report. It has a couple of advantages:

- The table content is defined where it's relevant in the document.
- Reordering the content of the document will also reorder the summary table.
- Tables can be dynamically generated using Jinja2 for loops.

This is a table built programmatically below with the following code:

{% raw %}
```
{{ ca.display_table("mytable", header=["Col 1", "Col 2", "Col 3"], headings="top") }}
```
{% endraw %}

{{ ca.display_table("mytable", header=["Col 1", "Col 2", "Col 3"], headings="top") }}

The actual rows are inputted below.

{% raw %}
```
{{ ca.table_row("mytable", ["First row!", "Let's do this!", "OK"]) }}
{{ ca.table_row("mytable", ["Second row!", "Another!", "Yeah!"]) }}
```
{% endraw %}

{{ ca.table_row("mytable", ["First row!", "Let's do this!", "OK"]) }}
{{ ca.table_row("mytable", ["Second row!", "Another!", "Yeah!"]) }}


{%- if vulnerabilities and not hide_vulnerabilities %}

{%- for scope in vulnerabilities.scopes %}

{{ new_section() }}

{%- if scope.name %}

# Detailed Vulnerabilities: {{ scope.name }}

{%- else %}

# Detailed Vulnerabilities

{%- endif %}

{{ scope.introduction }}

{%- for vulnerability in scope.vulnerabilities %}

## Vulnerability {{vulnerability.ref}}: {{vulnerability.name}}

{{ table([['Likelihood', 'Impact', 'Risk'], [vulnerability.headers['likelihood'], vulnerability.headers['impact'], vulnerability.risk]],
headings='top', cellstyles=[[],['','',ra_style_text]], colalign=['c','c','c']) }}

{{ vulnerability.markdown }}

{{ newpage() }}

{%- endfor %}

{%- endfor %}

{%- endif %}


{%- set appendices = import_section('appendices', ordinal='A') %}

{%- if len(appendices) > 0 and not hide_appendices %}

{{ new_section() }}
# Appendices {-}

{%- for appendix in appendices %}

## Appendix {{appendix['ref']}}: {{appendix['name']}} {-}

{{ appendix['markdown'] }}

{{ new_page() }}

{%- endfor %}

{%- endif %}

