---
title: Report Ranger sample report
client: Ranging Automation
template: sample
author: Volkis
authoremail: info@volkis.com.au
authormobile: "Mobile Number"
vulndir: "vulnerabilities"
appendixdir: "appendices"
changes:
- [0.1, 2020-11-26, "Matthew Strahan", "Initial sample report"]
contributors:
- ["Matthew Strahan", "Volkis Managing Director", "mobile", "info@volkis.com.au"]
updated_date: today
---

{{ of.newsection() }}

# Overview

This is an overview of how to display retesting results. This report has the header `updated_date: today` which overlays the updates on the vulnerabilities. Note the vulnerabilities themselves don't change order.

The following displays a table of vulnerability results:

{# Display the lists of vulnerabilities #}

{% for scope, vulnlist in vulnerabilities.get_scope_list().items() %}

{% if scope != '' %}

### {{scope}} {-}

{% endif %}

{{
  table(vulnlist,
    headings='top',
    header=['#','Title and Summary','Original Risk Rating', 'Current Status', 'Current Risk'],
    colpicker=['ref', 'title_and_summary', 'originalrisk', 'status', 'risk'],
    colalign=['c','l','c','c','c'],
    colwidths=[4, 42, 18, 18, 18],
    style_text = ra_style_text,
    append_column = {'title_and_summary': '{% if "notes" in headers %}**{{name}}** - {{headers["notes"]}}{%else%}{{name}}{%endif%}'})

}}

{% endfor %}




